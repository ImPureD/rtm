//исходный код приёмника данных о температуре ЦП
import std.stdio : writeln;
import std.socket;
import std.process;
import std.datetime;
import std.getopt;
import std.string;
import std.array;
import std.algorithm;

string parseCoreInfo(string output) {
	auto r = findSplit(output, "Core");

	return (r[1] ~ r[2]);
}

void main(string[] args) {
	int durationInMS = 1000;
	string ip = "87.103.197.23:2015";
	string name = "Sender";

	auto result = getopt(args,
		"d", "duration", &durationInMS,
		"i", "ip", &ip,
		"n", "name of sender", &name);

	if(result.helpWanted) {
		defaultGetoptPrinter("Some info about the program.", result.options);
	}

	writeln("Duration setted on ", durationInMS, "seconds");

	auto address = new InternetAddress(2015);
	address.parse(ip);

	auto listenerSocket = new TcpSocket();

	listenerSocket.connect(address);

	writeln("Connection established!");

	auto received = listenerSocket.send(name);

	if(received == Socket.ERROR) {
		writeln("Error");
	}

	auto ct = Clock.currTime;

	for(;;) {
		auto nt = Clock.currTime;

		auto dr = (nt - ct);

		if(dr.total!"msecs" >= durationInMS) {
			ct = Clock.currTime;

			auto sensors = execute(["sensors", "-u"]);

			string buffer = parseCoreInfo(sensors.output);
			received = listenerSocket.send(buffer);

			if(received == Socket.ERROR) {
				writeln("Error");
			}
		}
	}

	scope(exit) {
		listenerSocket.close();
	}
}